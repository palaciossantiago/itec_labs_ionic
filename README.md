# README #

Este proyecto fue creado con fines educativos para su presentación en las Jornadas Usinas Informáticas - TecnoArte - 2016 en el iTec.

### Requisitos ###

* Es necesario tener instalado [NodeJs](https://nodejs.org/en/)
* Una app(web) que sirva la capa api a utilizar. [App Demo](http://guia-comercial.data4.com.ar/api/shop)

### Instalación/Clonado Ionic  ###

* npm install -g cordova ionic (este comando es para instalar Ionic en sus pc's)
* Clonar este repositorio
* Iniciar el servidor de desarrollo [dir_clonado]/ ionic serve  o [dir_clonado]/ ionic serve --lab

### Autores ###

* Palacios, Santiago [@santypalacios](https://twitter.com/santypalacios)
* Fessia, Matias [@matiasfessia](https://twitter.com/mfessia)

### Agradecimientos ###
 
* A los directivos y el área técnica de ITEC, por el espacio, la confianza y la predisposición.
* A las personas que asistieron a la presentación.

** Hasta el próximo encuentro!!! **