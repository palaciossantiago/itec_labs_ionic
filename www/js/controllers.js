angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $ionicPopup, $timeout) {
  $scope.authors = [{
    id: 0,
    name: 'Palacios, Santiago',
    phone: '+(54)93584121743',
    twitter: 'santypalacios',
    facebook: 'palaciossantiago',
    email:'palaciossantiago@gmail.com',
    img:'img/santi_circle.png'
  }, {
    id: 1,
    name: 'Fessia, Matias',
    phone: '+(54)93584121743',
    twitter: 'mfessia',
    facebook: 'mfessia',
    email:'matiasfessia@gmail.com',
    img:'img/mati_circle.png'
  }];

  // An alert dialog
  $scope.showAlert = function(author) {
    var template = '<div class="list card pop-up"><a href="https://twitter.com/'+ author.twitter +'" target="_blank" class="item item-icon-left"> <i class="icon ion-social-twitter"></i>'+ author.twitter +'</a> <a href="https://www.facebook.com/'+author.facebook+'" target="_blank" class="item item-icon-left"> <i class="icon ion-social-facebook"></i> ' +author.facebook+'</a> <a href="mail:'+ author.email +'" class="item item-icon-left"> <i class="icon ion-email"></i> ' +author.email+'</a> <a href="tel:'+author.phone+'" class="item item-icon-left"> <i class="icon ion-iphone"></i> '+author.phone+'</a></div>';
    
/*      
    
      <a href="#" class="item item-icon-left">
        <i class="icon ion-email"></i>
        Enter card information
      </a>
      
      <a href="#" class="item item-icon-left">
        <i class="icon ion-iphone"></i>
        {{ author.name }}
      </a>
    </div>';*/

    var alertPopup = $ionicPopup.show({
      title: '<h4>'+ author.name +'</h4>',
      template: template,
      cssClass: 'pop-home-authors',
      buttons :[{
        text: '<b>OK</b>',
        type: 'button-positive',
        onTap: function(e) {
          alertPopup.close();
        }
      }]
   });

   alertPopup.then(function(res) {
     //console.log('Thank you for not eating my delicious ice cream cone');
   });
 };

})
  

.controller('ShopsCtrl', function($scope, $http, serverURL) {
  $http.get(serverURL+'/api/shop')
    .success(function(data) {
      $scope.shops = data.result;
    })
    .error(function(err) {
      console.log(err);
    })
  $scope.doRefresh = function() {
    $http.get(serverURL+'/api/shop')
      .success(function(data) {
        $scope.shops = data.result;
      })
      .finally(function() {
        // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      })
  }
})

.controller('ShopDetailCtrl', function($scope, $http, $stateParams, serverURL) {
  console.log('yeahhh');
  $http.get(serverURL+'/api/shop/' + $stateParams.id)
    .success(function(data) {
      $scope.shop = data.result;
    })
    .error(function(err) {
      console.log(err);
    });

})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
  $scope.notify = {
    enableNotify: false
  };
  $scope.geo = {
    enableGeoPos: true
  };
});
